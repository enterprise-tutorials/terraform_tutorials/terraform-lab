variable "aws_region" {
    description = "The AWS region where instance should be created."
    default = "us-east-1"
}

variable "aws_profile" {
    description = "AWS profile mentioned in credentials file."
    default = "awslearn-lion"
}

/******
  This section is used to
    a) Specify a particular TF version
    b) Keep TF state on S3 (bucket should be manually created before running the scripts)
  Note: You can comment this section if needed.
******/
/*terraform {
  required_version = "0.12.13"

  backend "s3" {
    bucket = "terraform-labs01"
    key = "tests/terraform/terraform.tfstate"
    region = "us-east-1"
    encrypt = true
  }
}*/
