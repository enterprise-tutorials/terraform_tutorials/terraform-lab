provider "aws" {
  version = "~> 2.39"
  shared_credentials_file = "$HOME/.aws/credentials"
  profile = "michael-efk"
  region = "${var.region}"
}

provider "template" {
  version = "~> 1.0"
}

terraform {
  required_version = "0.11.13"
}