output "app_endpoint" {
  value = "${aws_elb.app.dns_name}"
}
