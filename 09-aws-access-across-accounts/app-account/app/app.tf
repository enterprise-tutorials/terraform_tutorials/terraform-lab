resource "aws_elb" "app" {
  name = "app-elb"
  security_groups = [
    "${var.load_balancers_id}"]
  subnets = [
    "${var.subnet_id}"]

  listener {
    lb_protocol = "http"
    lb_port = 80

    instance_protocol = "http"
    instance_port = 8080
  }

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:8080/"
    interval = 5
  }

  cross_zone_load_balancing = true
}

data "template_file" "app" {
  template = "${file("${path.module}/files/app_container_definition.json.tpl")}"

  vars = {
    container_image = "${var.app_container_image}"
    fluentd_address = "${var.fluentd_address}"
  }
}

resource "aws_ecs_task_definition" "app" {
  family = "app"
  container_definitions = "${data.template_file.app.rendered}"
}

resource "aws_ecs_service" "app" {
  name = "app"
  cluster = "${var.ecs_cluster_name}"
  task_definition = "${aws_ecs_task_definition.app.arn}"
  iam_role = "${var.ecs_service_role_arn}"
  desired_count = 1
  depends_on = [
    "aws_elb.app"]

  load_balancer {
    elb_name = "${aws_elb.app.id}"
    container_name = "app"
    container_port = 8080
  }
}
