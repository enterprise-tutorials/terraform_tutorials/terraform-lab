data "terraform_remote_state" "efk" {
  backend = "s3"
  config = {
    bucket = "terraform-state-efk-new"
    key = "automation/terraform/efk.tfstate"
    region = "us-east-1"
    shared_credentials_file = "$HOME/.aws/credentials"
    profile = "default"
  }
}

module "ecs" {
  source = "../modules/ecs"

  region = "${var.region}"
  ecs_cluster_name = "app_cluster"
}

module "app" {
  source = "./app"

  region = "${var.region}"
  fluentd_address = "${data.terraform_remote_state.efk.fluentd_address.value}"

  ecs_cluster_name = "${module.ecs.ecs_cluster_name}"
  load_balancers_id = "${module.ecs.load_balancers_id}"
  subnet_id = "${module.ecs.subnet_id}"
  ecs_service_role_arn = "${module.ecs.ecs_service_role_arn}"
}