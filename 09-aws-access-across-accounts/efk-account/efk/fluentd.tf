module "ecs" {
  source = "../../modules/ecs"

  region = "${var.region}"
  ecs_cluster_name = "${var.ecs_cluster_name}"
}

data "aws_iam_policy_document" "ecs_task_role" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "fluentd_container_role" {
  name = "fluentdContainerRole"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_task_role.json}"
}

resource "aws_elb" "app" {
  name = "fluentd-elb"
  security_groups = [
    "${module.ecs.load_balancers_id}"]
  subnets = [
    "${module.ecs.subnet_id}"]

  listener {
    lb_protocol = "tcp"
    lb_port = "${var.fluentd_listening_port}"

    instance_protocol = "tcp"
    instance_port = "${var.fluentd_listening_port}"
  }

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 2
    timeout = 3
    target = "TCP:${var.fluentd_listening_port}"
    interval = 5
  }

  cross_zone_load_balancing = true
}

data "template_file" "container_definitions" {
  template = "${file("${path.module}/files/fluentd_container_definition.json.tpl")}"

  vars = {
    conatiner_image = "${var.fluentd_image}"
    es_endpoint = "${aws_elasticsearch_domain.domain.endpoint}"
    es_region = "${var.region}"
    log_flush_interval = "${var.log_flush_interval}"
    listening_port = "${var.fluentd_listening_port}"
  }
}

resource "aws_ecs_task_definition" "fluentd_task" {
  family = "fluentd-task"
  task_role_arn = "${aws_iam_role.fluentd_container_role.arn}"

  container_definitions = "${data.template_file.container_definitions.rendered}"
}

resource "aws_ecs_service" "fluentd_service" {
  name = "fluentd-service"
  cluster = "${var.ecs_cluster_name}"
  task_definition = "${aws_ecs_task_definition.fluentd_task.arn}"
  iam_role = "${module.ecs.ecs_service_role_arn}"
  desired_count = 1

  depends_on = [
    "aws_elb.app"]

  load_balancer {
    elb_name = "${aws_elb.app.id}"
    container_name = "fluentd"
    container_port = "${var.fluentd_listening_port}"
  }
}
