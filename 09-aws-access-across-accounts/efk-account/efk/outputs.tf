output "es_endpoint" {
  value = "${aws_elasticsearch_domain.domain.endpoint}"
}

output "kibana_endpoint" {
  value = "${aws_elasticsearch_domain.domain.kibana_endpoint}"
}

output "fluentd_address" {
  value = "${aws_elb.app.dns_name}:${var.fluentd_listening_port}"
}