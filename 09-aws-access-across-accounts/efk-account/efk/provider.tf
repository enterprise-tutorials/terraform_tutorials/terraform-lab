provider "aws" {
  version = "~> 2.39"
  shared_credentials_file = "$HOME/.aws/credentials"
  profile = "default"
  region = "${var.region}"
}

provider "template" {
  version = "~> 1.0"
}

terraform {
  required_version = "0.11.13"

  backend "s3" {
    bucket = "terraform-state-efk-new"
    key = "automation/terraform/efk.tfstate"
    region = "us-east-1"
    encrypt = true
  }
}