data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_elasticsearch_domain" "domain" {
  domain_name = "${var.es_domain}"
  elasticsearch_version = "7.1"

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": {
                "AWS": "*"
            },
            "Effect": "Allow",
            "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.es_domain}/*"
        }
    ]
}
CONFIG

  cluster_config {
    instance_type = "t2.small.elasticsearch"
    // Data node instance type
    instance_count = 1
    // Number of data nodes
    dedicated_master_enabled = false
    // true for production
    dedicated_master_count = 0
    // Number of master nodes
    dedicated_master_type = "t2.small.elasticsearch"
    // Master node instance type
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = {
    Domain = "${var.es_domain}"
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
    // in GB per node
  }
}
