resource "aws_s3_bucket" "remote_state_bucket" {
  bucket = "${var.bucket_name}"
  acl = "private"
  region = "${var.region}"
  force_destroy = true

  versioning {
    enabled = true
  }
}

data "aws_partition" "current" {
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "bucket_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      "arn:${data.aws_partition.current.partition}:s3:::${aws_s3_bucket.remote_state_bucket.id}"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:*Object"
    ]

    resources = [
      "arn:${data.aws_partition.current.partition}:s3:::${aws_s3_bucket.remote_state_bucket.id}/*"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = "${aws_s3_bucket.remote_state_bucket.id}"
  policy = "${data.aws_iam_policy_document.bucket_policy_document.json}"
}
