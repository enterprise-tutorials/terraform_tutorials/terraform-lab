variable "aws_region" {
  description = "The AWS region to create resources in."
  default = "us-east-1"
}

variable "availability_zone" {
  description = "The availability zone"
  default = "us-east-1a"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
  default = "test-ecs-cluster"
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the AWS ECS optimized images."
  default = {
    us-east-1 = "ami-00129b193dc81bc31"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "prefix" {
  default = "test"
}

variable "ssh_pubkey_file" {
  description = "Path to an SSH public key"
  default = "~/.ssh/id_rsa_gitlab_personal.pub"
}
