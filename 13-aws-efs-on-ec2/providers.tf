provider "aws" {
  version = "~> 2.39"
  shared_credentials_file = "$HOME/.aws/credentials"
  profile = "awslearn-lion"
  region = var.aws_region
}
