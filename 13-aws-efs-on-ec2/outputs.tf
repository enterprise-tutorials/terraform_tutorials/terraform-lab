output "load_balancers_id" {
  value = "${aws_security_group.load_balancers.id}"
}

output "subnet_id" {
  value = "${aws_subnet.main.id}"
}

output "ecs_service_role_arn" {
  value = "${aws_iam_role.ecs_service_role.arn}"
}

output "ecs_cluster_name" {
  value = "${var.ecs_cluster_name}"
}
