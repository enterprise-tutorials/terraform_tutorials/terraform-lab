resource "aws_key_pair" "awslearn" {
  key_name = "awslearn-key"
  public_key = file(var.ssh_pubkey_file)
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "main" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = var.availability_zone
}

resource "aws_route_table" "external" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

resource "aws_route_table_association" "external-main" {
  subnet_id = aws_subnet.main.id
  route_table_id = aws_route_table.external.id
}

resource "aws_security_group" "load_balancers" {
  name = "load_balancers"
  description = "Allows all traffic"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ecs" {
  name = "ecs"
  description = "Allows all traffic"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = ["${aws_security_group.load_balancers.id}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "ecs-cluster" {
  name = var.ecs_cluster_name
  min_size = 1
  max_size = 2
  desired_capacity = 1
  health_check_type = "EC2"
  launch_configuration = aws_launch_configuration.ecs.name
  vpc_zone_identifier = ["${aws_subnet.main.id}"]
}

data "template_file" "user_data" {
  template = "${file("${path.module}/files/user-data.sh")}"
  vars = {
    cluster_name = var.ecs_cluster_name
  }
}

resource "aws_launch_configuration" "ecs" {
  name = var.ecs_cluster_name
  image_id = lookup(var.amis, var.aws_region)
  instance_type = var.instance_type
  security_groups = [aws_security_group.ecs.id]
  iam_instance_profile = aws_iam_instance_profile.ecs.name
  key_name = aws_key_pair.awslearn.key_name
  associate_public_ip_address = true
  user_data = data.template_file.user_data.rendered
}

resource "aws_iam_role" "ecs_host_role" {
  name = "${var.prefix}-ecs_host_role"
  assume_role_policy = file("${path.module}/files/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name = "${var.prefix}-ecs_instance_role_policy"
  policy = file("${path.module}/files/ecs-instance-role-policy.json")
  role = aws_iam_role.ecs_host_role.id
}

resource "aws_iam_role" "ecs_service_role" {
  name = "${var.prefix}-ecs_service_role"
  assume_role_policy = file("${path.module}/files/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs_service_role_policy" {
  name = "${var.prefix}-ecs_service_role_policy"
  policy = file("${path.module}/files/ecs-service-role-policy.json")
  role = aws_iam_role.ecs_service_role.id
}

resource "aws_iam_instance_profile" "ecs" {
  name = "${var.prefix}-ecs-instance-profile"
  path = "/"
  role = aws_iam_role.ecs_host_role.name
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.ecs_cluster_name
}
