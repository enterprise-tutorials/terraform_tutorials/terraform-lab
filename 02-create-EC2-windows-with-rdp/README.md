# In this lesson you will be able to create a ec2 instance with WindowsOS and access with RDP.

I have set this example up on `us-east-1` region. If you change the region, make sure you change the `ami` as well.

# Steps to execute

### A. Setting AWS keys 
1. Open `ec2.tf` and set your AWS profile. The profile attribute refers to the AWS Config File in ~/.aws/credentials on MacOS and Linux or %UserProfile%\.aws\credentials on a Windows system. It is recommended practice that credentials never be hardcoded into *.tf configuration files. 
2. In `ec2.tf`, you will see `key-name` entry as *michaeltf*. Make sure your have a key-value pair added to access EC2 instances and that name is placed as key-name.


### B. Executing Scripts
1. ``cd`` into script directory
2. ``terraform init`` This is to initialize provider plugins locally
3. ``terraform validate`` will validate the script that you have written
3. ``terraform plan`` will display the settings what we are going to deploy
4. ``terraform apply`` This is to create the instances in AWS

*Note: Since we are setting up Windows here, AWS takes about 4-5 minutes to complete the initialization.*

After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. Execute ``terraform destroy`` to remove all the resources. **Important**: Do not run this command in production unless you want to tear down everything.
