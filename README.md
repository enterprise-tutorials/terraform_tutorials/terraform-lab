# Terraform Lab exercises
The goals is to show different examples on how Terraform scripts can be prepared so you can have a one-stop shop to learn Terraform quickly.

I have primarily focussed on examples with AWS. The AWS free tier for 1-year helps you to try out all these examples free of cost.

Note: I will not be showing here how to login/ssh/rdp into the instances you have created.

## IMPORTANT
* HashiCorp recommended practice that credentials never be hardcoded into *.tf configuration files.
* Verify that there are no other *.tf files in each *sub-directory* other than what you are trying to work on, since Terraform loads all of them during execution.
* Never ever save you AWS credentials if you plan to share your code publicly
* Never ever implement directly on production without first testing out

## Pre-requsites
* Should have a basic understanding on how to navigate on AWS
* Must have AWS credentials to create instances...preferably FullAccess
* Install Terraform 0.12.13

## References
* [Terraform by HashiCorp](https://www.terraform.io/docs/index.html)
* [Cloud-Yeti](https://github.com/Cloud-Yeti/aws-labs/tree/master/terraform-aws)
* And many others

## Questions/Comments/Feedbacks/Concerns
[Email](codercheerful@gmail.com)