output "es_endpoint" {
  value = aws_elasticsearch_domain.domain.endpoint
}

output "kibana_endpoint" {
  value = aws_elasticsearch_domain.domain.kibana_endpoint
}

