variable "domain" {
  default = "test-es"
  description = "The AWS ES domain name."
}