data "aws_iam_policy_document" "ecs_task_role" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "fluentbit_container_role" {
  name = "fluentbitContainerRole"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_role.json
}

data "template_file" "container_definitions" {
  template = file("templates/fluentbit.json")
}

resource "aws_ecs_task_definition" "fluentbit_task" {
  family = "fluentbit-task"
  task_role_arn = aws_iam_role.fluentbit_container_role.arn
  network_mode = "host"
  volume {
    name      = "socket"
    host_path = "/var/run"
  }
  container_definitions = data.template_file.container_definitions.rendered
}

resource "aws_ecs_service" "fluentbit_service" {
  name = "fluentbit-service"
  task_definition = aws_ecs_task_definition.fluentbit_task.arn
  cluster = var.ecs_cluster
  launch_type = "EC2"
  scheduling_strategy = "DAEMON"
}
