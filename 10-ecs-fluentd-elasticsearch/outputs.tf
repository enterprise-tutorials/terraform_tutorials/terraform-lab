output "es_endpoint" {
  value = module.aes.es_endpoint
}

output "kibana_endpoint" {
  value = module.aes.kibana_endpoint
}

output "app_endpoint" {
  value = module.app.app_endpoint
}

