variable "region" {
  default = "us-east-1"
  description = "The region of your infrastructure."
}

variable "ecs_cluster" {
  description = "The name of the ECS cluster to use."
  default = "test_cluster"
}

variable "es_endpoint" {
  description = "The AWS ES endpoint."
}

variable "es_region" {
  description = "The AWS ES region."
}

variable "log_flush_interval" {
  description = "The log flush interval."
  default = "1s"
}

variable "container_image" {
  description = "The container docker image."
  default = "xmsandhu/fluentd:stable"
}

variable "cpu" {
  description = "The allocated CPU."
  default = "256"
}

variable "memory" {
  description = "The allocated memory."
  default = "128"
}

variable "listening_port" {
  description = "The listening port."
  default = "24224"
}