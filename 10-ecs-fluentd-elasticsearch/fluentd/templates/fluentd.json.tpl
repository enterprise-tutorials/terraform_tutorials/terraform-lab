[
  {
    "name": "fluentd",
    "image": "${conatiner_image}",
    "cpu": ${cpu},
    "memory": ${memory},
    "essential": true,
    "portMappings": [
      {
        "containerPort": ${listening_port},
        "hostPort": ${listening_port}
      }
    ],
    "environment": [
      {
        "name": "ES_ENDPOINT",
        "value": "${es_endpoint}"
      },
      {
        "name": "ES_REGION",
        "value": "${es_region}"
      },
      {
        "name": "LOG_FLUSH_INTERVAL",
        "value": "${log_flush_interval}"
      },
      {
        "name": "LISTENING_PORT",
        "value": "${listening_port}"
      }
    ]
  }
]
