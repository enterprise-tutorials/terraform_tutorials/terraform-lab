data "aws_iam_policy_document" "ecs_task_role" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "fluentd_container_role" {
  name = "fluentdContainerRole"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_role.json
}

data "template_file" "container_definitions" {
  template = file("${path.module}/templates/fluentd.json.tpl")

  vars = {
    conatiner_image = var.container_image
    cpu = var.cpu
    memory = var.memory
    es_endpoint = var.es_endpoint
    es_region = var.es_region
    log_flush_interval = var.log_flush_interval
    listening_port = var.listening_port
  }
}

resource "aws_ecs_task_definition" "fluentd_task" {
  family = "fluentd-task"
  task_role_arn = aws_iam_role.fluentd_container_role.arn

  container_definitions = data.template_file.container_definitions.rendered
}

resource "aws_ecs_service" "fluentd_service" {
  name = "fluentd-service"
  task_definition = aws_ecs_task_definition.fluentd_task.arn
  cluster = var.ecs_cluster
  launch_type = "EC2"
  scheduling_strategy = "DAEMON"
}
