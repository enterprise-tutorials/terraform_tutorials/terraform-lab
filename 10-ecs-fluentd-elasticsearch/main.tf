module "aes" {
  source = "./aes"

  domain = var.es_domain_name
}

module "ecs" {
  source = "./ecs"

  create_ecs = var.create_ecs
}

module "app" {
  source = "./app"

  region = var.region
  ecs_cluster_name = module.ecs.ecs_cluster_name
  fluentd_address = "${var.fluentd_host}:${var.fluentd_port}"
}

module "fluentd" {
  source = "./fluentd"

  ecs_cluster = module.ecs.ecs_cluster_name
  es_endpoint = module.aes.es_endpoint
  es_region = var.region
  listening_port = var.fluentd_port
}
