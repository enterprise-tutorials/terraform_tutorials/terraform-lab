output "ecs_cluster_id" {
  value = concat(aws_ecs_cluster.ecs_cluster.*.id, [""])[0]
}

output "ecs_cluster_arn" {
  value = concat(aws_ecs_cluster.ecs_cluster.*.arn, [""])[0]
}

output "ecs_cluster_name" {
  value = var.cluster_name
}