variable "cluster_name" {
    description = "The name of the Amazon ECS cluster."
    default = "test_cluster"
}

variable "create_ecs" {
    description = "Controls if ECS should be created."
    type        = bool
    default     = true
}

variable "tags" {
    description = "A map of tags to add to ECS Cluster."
    type        = map(string)
    default     = {}
}