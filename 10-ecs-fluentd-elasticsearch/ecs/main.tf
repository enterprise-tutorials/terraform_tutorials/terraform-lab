resource "aws_ecs_cluster" "ecs_cluster" {
  count = var.create_ecs ? 1 : 0

  name = var.cluster_name
  tags = var.tags
}