[
  {
    "name": "app",
    "image": "${container_image}",
    "cpu": ${cpu},
    "memory": ${memory},
    "links": [],
    "logConfiguration": {
      "logDriver": "fluentd",
      "options": {
        "fluentd-async-connect": "true",
        "fluentd-address": "${fluentd_address}",
        "tag": "app"
      }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port},
        "protocol": "tcp"
      }
    ],
    "essential": true,
    "entryPoint": [],
    "command": [],
    "environment": [],
    "mountPoints": [],
    "volumesFrom": []
  }
]
