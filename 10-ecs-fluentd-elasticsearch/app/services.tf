resource "aws_elb" "app" {
    name = "app-elb"
    security_groups = [aws_security_group.load_balancers.id]
    subnets = [aws_subnet.main.id]

    listener {
        lb_protocol = "http"
        lb_port = 80

        instance_protocol = "http"
        instance_port = 8080
    }

    health_check {
        healthy_threshold = 3
        unhealthy_threshold = 2
        timeout = 3
        target = "HTTP:8080/"
        interval = 5
    }

    cross_zone_load_balancing = true
}

data "template_file" "app" {
    template = file("${path.module}/templates/app.json.tpl")

    vars = {
        container_image = var.container_image
        app_port = var.app_port
        cpu = var.cpu
        memory = var.memory
        fluentd_address = var.fluentd_address
    }
}

resource "aws_ecs_task_definition" "app" {
    family = "app"
    container_definitions = data.template_file.app.rendered
}

resource "aws_ecs_service" "app" {
    name = "app"
    cluster = var.ecs_cluster_name
    task_definition = aws_ecs_task_definition.app.arn
    iam_role = aws_iam_role.ecs_service_role.arn
    desired_count = 1
    depends_on = [aws_iam_role_policy.ecs_service_role_policy, aws_elb.app]

    load_balancer {
        elb_name = aws_elb.app.id
        container_name = "app"
        container_port = 8080
    }
}
