variable "region" {
  description = "The AWS region to create resources in."
}

variable "availability_zone" {
  description = "The availability zone"
  default = "us-east-1a"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
  default = "btv_cluster"
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the AWS ECS optimized images."
  default = {
    us-east-1 = "ami-00129b193dc81bc31"
  }
}

variable "autoscale_min" {
  default = "1"
  description = "Minimum autoscale (number of EC2)"
}

variable "autoscale_max" {
  default = "2"
  description = "Maximum autoscale (number of EC2)"
}

variable "autoscale_desired" {
  default = "1"
  description = "Desired autoscale (number of EC2)"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ssh_pubkey_file" {
  description = "Path to an SSH public key"
  default = "~/.ssh/id_rsa.pub"
}

variable "container_image" {
  description = "Docker image"
  default = "xmsandhu/demo-docker:latest"
}

variable "app_port" {
  description = "The app port"
  default = "8080"
}

variable "cpu" {
  description = "The CPU"
  default = "100"
}

variable "memory" {
  description = "The memory"
  default = "512"
}

variable "fluentd_address" {
  description = "The fluentd address."
  default = "127.0.0.1:24224"
}

