variable "region" {
  default = "us-east-1"
  description = "The region of your infrastructure."
}

variable "es_domain_name" {
  default = "test-es"
  description = "The AWS ES domain name."
}

variable "create_ecs" {
  description = "Controls if ECS should be created."
  type        = bool
  default     = true
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
  default = "test_cluster"
}

variable "fluentd_host" {
  description = "The fluentd host."
  default = "127.0.0.1"
}

variable "fluentd_port" {
  description = "The fluentd listening port."
  default = "24224"
}