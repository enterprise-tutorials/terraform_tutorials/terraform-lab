# Scripts to push logs from ECS containers to EFK

The scripts will deploy the AWS services/instances in the following order:
1. Amazon ES & ECS cluster
2. The demo application on ECS
3. The Fluentd on ECS

### Prerequisites
1. The docker image for the Fluentd should be available on docker hub. Following are the steps to build and push the docker image to docker hub:
    1. Change directory: ``cd efk-on-ecs/fluentd/image``
    2. Build the docker image: ``docker build -it <image_name>:<image_tag> .``
    3. Push the image to docker hub: ``docker push <image_name>:<image_tag>``
    4. Update the variable ``container_image`` in ``fluentd/variables.tf`` file with the new image tag. 
2. This example uses a simple spring boot application as the demo application. The docker image for the application is available as ``xmsandhu/demo-docker:latest``. If you need to deploy your own application, then update the variable ``container_image`` in ``app/variables.tf`` file with the new image tag.  

### Steps to execute the scripts
1. ``cd efk-on-ecs`` - Change directory to efk-on-ecs
2. ``terraform init`` - Initialize a working directory
3. ``terraform plan`` - Create an execution plan
4. ``terraform apply`` - Apply the changes

### Outputs:
1. ``es_endpoint`` - The Amazon Elasticsearch Service URL
2. ``kibana_endpoint`` - The Kibana URL
3. ``app_endpoint`` - The demo application URL
     
### How to generate logs from the demo application?
Enter ``<app_endpoint>/<anything>`` to generate a log.

Search for the log on Kibana using the filter: ``log is <anything>``

After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. Execute ``terraform destroy`` from ``efk-on-ecs`` directory to remove all the resources. **Important**: Do not run this command in production unless you want to tear down everything.
