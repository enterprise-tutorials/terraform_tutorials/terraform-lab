# VPC
resource "aws_vpc" "terra_vpc" {
    cidr_block       = "${var.vpc_cidr}"
    enable_dns_hostnames = true
    enable_dns_support = true
    tags = {
        Name = "tf-VPC"
    }
}

# Internet Gateway
resource "aws_internet_gateway" "terra_igw" {
    vpc_id = "${aws_vpc.terra_vpc.id}"
    tags = {
        Name = "tf-igw-main"
    }
}

# Subnets : public
resource "aws_subnet" "public_sn" {
    count = "${length(var.public_subnets_cidr)}"
    vpc_id = "${aws_vpc.terra_vpc.id}"
    cidr_block = "${element(var.public_subnets_cidr,count.index)}"
    availability_zone = "${lookup(var.availability_zone, var.aws_region)}"
    tags = {
        Name = "tf-public-subnet-${count.index+1}"
    }
}

# Private subnet
/*resource "aws_subnet" "private_sn" {
    vpc_id = "${aws_vpc.terra_vpc.id}"
    cidr_block = "${var.private_subnets_cidr}"
#    availability_zone = "${lookup(var.availability_zone, "us-west-2")}"
    availability_zone = "us-east-1a"
    tags = {
        Name = "tf_vpc_private_sn"
    }
}*/

# Route table: attach Internet Gateway
resource "aws_route_table" "public_rt" {
    vpc_id = "${aws_vpc.terra_vpc.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.terra_igw.id}"
    }
    tags = {
        Name = "tf-publicRouteTable"
    }
}

# Route table association with public subnets
resource "aws_route_table_association" "subnet_association" {
    count = "${length(var.public_subnets_cidr)}"
    subnet_id      = "${element(aws_subnet.public_sn.*.id,count.index)}"
    route_table_id = "${aws_route_table.public_rt.id}"
}
