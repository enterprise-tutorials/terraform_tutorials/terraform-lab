/*resource "aws_instance" "test-ec2-instance" {
    ami = "${var.ami_id}"
    instance_type = "${var.instance_type}"
#    key_name = "${var.aws_profile}"
    security_groups = ["${aws_security_group.ingress-all.id}"]
#    security_groups = ["${aws_security_group.selected-ones.id}"]
    tags = {
        Name = "tf-${var.instance_type}"
    }
    associate_public_ip_address = true

    count = "${length(var.public_subnets_cidr)}"
    subnet_id = "${element(aws_subnet.public_sn.*.id,count.index)}"
}*/

resource "aws_instance" "test-ec2-instance" {
    ami = "${var.ami_id}"
    instance_type = "${var.instance_type}"

    count = "${length(var.public_subnets_cidr)}"
    subnet_id      = "${element(aws_subnet.public_sn.*.id,count.index)}"
#    subnet_id = "${aws_subnet.public_sn.id}"

    vpc_security_group_ids = ["${aws_security_group.allow_http.id}"]
    associate_public_ip_address = true

    user_data = <<-EOF
                #!/bin/bash
                echo "Hello, World" > index.html
                nohup busybox httpd -f -p 8080 &
                EOF
    tags = {
        Name = "tf-${var.instance_type}"
    }
}