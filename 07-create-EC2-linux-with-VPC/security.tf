###########
# There are 2 types of SecurityGroups in this file - all and limited.
# Use only any one of them at a time.
#
##########
/*resource "aws_security_group" "ingress-all" {
    name = "tf-allow-all-sg"
    description = "Allow all inbound traffic"
    vpc_id = "${aws_vpc.terra_vpc.id}"

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port = 22
        to_port = 8080
        protocol = "tcp"
    }

    // Terraform removes the default rule
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "tf-allow-all-sg"
    }
}*/

resource "aws_security_group" "allow_http" {
    description = "Allow inbound HTTP traffic for ${aws_vpc.terra_vpc.id} instance"
    vpc_id      = "${aws_vpc.terra_vpc.id}"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

/*
resource "aws_security_group" "ingress-limited" {
    name = "tf-allow-selected"
    description = "lab security group to access private ports"
    vpc_id = "${aws_vpc.terra_vpc.id}"

    # allow memcached port within VPC
    ingress {
        from_port = 11211
        to_port = 11211
        protocol = "tcp"
        cidr_blocks = [
        "${var.public_subnets_cidr}"]
    }

    # allow redis port within VPC
    ingress {
        from_port = 6379
        to_port = 6379
        protocol = "tcp"
        cidr_blocks = [
        "${var.public_subnets_cidr}"]
    }

    # allow postgres port within VPC
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = [
        "${var.public_subnets_cidr}"]
    }

    # allow mysql port within VPC
    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = [
        "${var.public_subnets_cidr}"]
    }

    egress {
        from_port = "0"
        to_port = "0"
        protocol = "-1"
        cidr_blocks = [
        "0.0.0.0/0"]
    }
    tags = {
        Name = "tf-limited-sg"
    }
}
*/
