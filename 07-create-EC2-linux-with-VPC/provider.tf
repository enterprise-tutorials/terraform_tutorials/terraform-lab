# Define AWS as our provider
provider "aws" {
    profile = "${var.aws_profile}"
    region = "${var.aws_region}"
}