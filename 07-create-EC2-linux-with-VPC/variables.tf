variable "aws_profile" {
    description = "AWS profile mentioned in credentials file"
    default = "bird-lion"
}

variable "instance_type" {
    description = "AWS Instance"
    default = "t2.micro"
}

variable "ami_id" {
    description = "AWS of the instance"
    default = "ami-2757f631"
}

variable "aws_region" {
    description = "Region for the VPC"
    default = "us-east-1"
}

/*
CIDR blocks are a concise way to specify IP address ranges.
For example, a CIDR block of 10.0.0.0/24 represents all IP addresses
between 10.0.0.0 and 10.0.0.255.
The CIDR block 0.0.0.0/0 is an IP address range that includes all
possible IP addresses
Read: https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180
*/

variable "vpc_cidr" {
    description = "CIDR for the VPC"
    default = "10.0.0.0/24"
}

variable "public_subnets_cidr" {
	type = "list"
	default = ["10.0.0.0/24"]
}

/*variable "private_subnets_cidr" {
	type = "string"
	default = "10.0.0.1/32"
}*/

variable "availability_zone" {
    description = "availability zone used for the lab, based on region"
	default = {
        us-east-1 = "us-east-1a"
        us-west-2 = "us-west-2a"
    }
}
