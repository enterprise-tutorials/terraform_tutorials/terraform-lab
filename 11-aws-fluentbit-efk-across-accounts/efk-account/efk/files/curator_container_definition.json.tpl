[
  {
    "name": "es-curator",
    "image": "${container_image}",
    "cpu": 100,
    "memory": 128,
    "essential": true,
    "environment": [
      {
        "name": "ES_HOST",
        "value": "${es_host}"
      }
    ]
  }
]
