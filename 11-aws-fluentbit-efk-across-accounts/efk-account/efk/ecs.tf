module "ecs" {
  source = "../../modules/ecs"

  region = "${var.region}"
  ecs_cluster_name = "${local.ecs_cluster_name}"
  enable_fluentbit = "false"
}