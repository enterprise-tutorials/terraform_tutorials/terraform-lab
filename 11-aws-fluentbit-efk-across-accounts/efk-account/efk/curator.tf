data "template_file" "es_curator" {
  template = "${file("${path.module}/files/curator_container_definition.json.tpl")}"

  vars = {
    es_host = "${aws_elasticsearch_domain.domain.endpoint}"
    container_image = "${var.es_curator_image}"
  }
}

resource "aws_ecs_task_definition" "es_curator" {
  family = "es_curator"
  container_definitions = "${data.template_file.es_curator.rendered}"
}

resource "aws_ecs_service" "es_curator" {
  name = "es_curator"
  cluster = "${local.ecs_cluster_name}"
  task_definition = "${aws_ecs_task_definition.es_curator.arn}"
  desired_count = 1
}
