output "es_endpoint" {
  value = "${aws_elasticsearch_domain.domain.endpoint}"
}

output "kibana_endpoint" {
  value = "${aws_elasticsearch_domain.domain.kibana_endpoint}"
}

output "fluentd_host" {
  value = "${aws_elb.app.dns_name}"
}

output "fluentd_port" {
  value = "${var.fluentd_listening_port}"
}