locals {
  es_doamin = "${terraform.workspace}-${var.es_domain}"
  ecs_cluster_name = "${terraform.workspace}-${var.ecs_cluster_name}"
  fluentd_elb_name = "${terraform.workspace}-fluentd-elb"
  fluentd_container_role_name = "${terraform.workspace}-container-role"
}
