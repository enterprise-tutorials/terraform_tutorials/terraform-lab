variable "es_domain" {
  default = "test-es"
  description = "The AWS ES domain name."
}

variable "ecs_cluster_name" {
  default = "efk_cluster"
  description = "The name of the ECS cluster to use."
}

variable "region" {
  default = "us-east-1"
  description = "The region of your infrastructure."
}

variable "log_flush_interval" {
  default = "1s"
  description = "The log flush interval."
}

variable "fluentd_image" {
  default = "xmsandhu/fluentd:json"
  description = "The container docker image."
}

variable "es_curator_image" {
  default = "xmsandhu/es_curator:1.2"
  description = "The container docker image."
}

variable "fluentd_listening_port" {
  default = "24224"
  description = "The listening port."
}