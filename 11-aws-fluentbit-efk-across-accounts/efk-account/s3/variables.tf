variable "bucket_name" {
  default = "terraform-state-efk"
  description = "the name to give the bucket"
}

variable "region" {
  default = "us-east-1"
  description = "Region where the S3 bucket will be created"
}