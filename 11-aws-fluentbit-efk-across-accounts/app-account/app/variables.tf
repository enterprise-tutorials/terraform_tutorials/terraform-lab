variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
}

variable "app_container_image" {
  description = "Docker image"
  default = "xmsandhu/demo-docker:latest"
}

variable "fluentbit_address" {
  description = "The FluentD address."
}

variable "load_balancers_id" {
  description = "The id of the load balancer."
}

variable "subnet_id" {
  description = "The id of the subnet."
}

variable "ecs_service_role_arn" {
  description = "The ECS service role arn."
}

variable "app_elb_name" {
  description = "The app elb name."
}