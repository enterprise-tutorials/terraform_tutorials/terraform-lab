[
  {
    "name": "app",
    "image": "${container_image}",
    "cpu": 100,
    "memory": 512,
    "links": [],
    "logConfiguration": {
      "logDriver": "fluentd",
      "options": {
        "fluentd-async-connect": "true",
        "fluentd-address": "${fluentd_address}",
        "tag": "app"
      }
    },
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080,
        "protocol": "tcp"
      }
    ],
    "essential": true,
    "entryPoint": [],
    "command": [],
    "environment": [],
    "mountPoints": [],
    "volumesFrom": []
  }
]
