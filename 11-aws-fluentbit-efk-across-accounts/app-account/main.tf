module "ecs" {
  source = "../modules/ecs"

  region = "${var.region}"
  ecs_cluster_name = "${local.ecs_cluster_name}"
}

module "app" {
  source = "./app"

  fluentbit_address = "${var.fluentbit_host}:${var.fluentbit_port}"
  ecs_cluster_name = "${module.ecs.ecs_cluster_name}"
  load_balancers_id = "${module.ecs.load_balancers_id}"
  subnet_id = "${module.ecs.subnet_id}"
  ecs_service_role_arn = "${module.ecs.ecs_service_role_arn}"
  app_elb_name = "${local.app_elb_name}"
}
