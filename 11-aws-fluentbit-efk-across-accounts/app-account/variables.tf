variable "region" {
  default = "us-east-1"
  description = "The region of your infrastructure."
}

variable "fluentbit_host" {
  default = "localhost"
  description = "The fluentbit host."
}

variable "fluentbit_port" {
  default = "24224"
  description = "The fluentbit port."
}
