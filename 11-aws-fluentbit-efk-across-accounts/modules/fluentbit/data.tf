data "terraform_remote_state" "efk" {
  backend = "s3"
  workspace = "default"
  config = {
    bucket = "terraform-state-efk"
    key = "automation/terraform/efk.tfstate"
    region = "us-east-1"
    shared_credentials_file = "$HOME/.aws/credentials"
    profile = "michael-efk"
  }
}