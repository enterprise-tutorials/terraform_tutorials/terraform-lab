[
  {
    "name": "fluentbit",
    "image": "${container_image}",
    "cpu": 100,
    "memory": 128,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 24224,
        "hostPort": 24224
      }
    ],
    "environment": [
      {
        "name": "LISTENING_PORT",
        "value": "24224"
      },
      {
        "name": "FLUENTD_HOST",
        "value": "${fluentd_host}"
      },
      {
        "name": "FLUENTD_PORT",
        "value": "${fluentd_port}"
      },
      {
        "name": "ENV",
        "value": "${env}"
      }
    ]
  }
]
