data "template_file" "fluentbit" {
  count = "${var.enable_fluentbit == "true" ? 1 : 0 }"
  template = "${file("${path.module}/files/fluentbit_container_definition.json.tpl")}"

  vars = {
    container_image = "${var.fluentbit_container_image}"
    fluentd_host = "${data.terraform_remote_state.efk.fluentd_host.value}"
    fluentd_port = "${data.terraform_remote_state.efk.fluentd_port.value}"
    env = "${var.env}"
  }
}

resource "aws_ecs_task_definition" "fluentbit" {
  count = "${var.enable_fluentbit == "true" ? 1 : 0 }"
  family = "fluentbit"
  container_definitions = "${data.template_file.fluentbit.rendered}"
}

resource "aws_ecs_service" "fluentbit" {
  count = "${var.enable_fluentbit == "true" ? 1 : 0 }"
  name = "fluentbit"
  cluster = "${var.ecs_cluster_name}"
  task_definition = "${aws_ecs_task_definition.fluentbit.arn}"
  scheduling_strategy = "DAEMON"
}
