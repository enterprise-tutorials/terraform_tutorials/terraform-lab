variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
}

variable "fluentbit_container_image" {
  description = "Docker image"
  default = "xmsandhu/fluentbit:json-1.0"
}

variable "enable_fluentbit" {
  default = "true"
}

variable "env" {
  description = "Environment tag."
}