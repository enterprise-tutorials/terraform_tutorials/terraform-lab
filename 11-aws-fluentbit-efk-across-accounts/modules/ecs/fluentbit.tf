module "fluentbit" {

  source = "../fluentbit"

  ecs_cluster_name = "${var.ecs_cluster_name}"

  enable_fluentbit = "${var.enable_fluentbit}"

  env = "${terraform.workspace}"
}