variable "region" {
  description = "The AWS region to create resources in."
}

variable "availability_zone" {
  description = "The availability zone"
  default = "us-east-1a"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the AWS ECS optimized images."
  default = {
    us-east-1 = "ami-00129b193dc81bc31"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "enable_fluentbit" {
  default = "true"
}