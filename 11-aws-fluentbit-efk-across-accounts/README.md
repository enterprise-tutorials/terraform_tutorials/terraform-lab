# Access Fluent Bit + EFK services across different accounts

The goal is to access an EFK cluster that was created in a different AWS account using Fluent Bit.

### Prerequisites
1. The docker image for the Fluentd should be available on docker hub. Following are the steps to build and push the Fluentd docker image to docker hub:
    1. Change directory: ``cd efk-account/efk/fluentd-image``
    2. Build the docker image: ``docker build -it <image_name>:<image_tag> .``
    3. Push the image to docker hub: ``docker push <image_name>:<image_tag>``
    4. Update the variable ``fluentd_image`` in ``efk-accoun/efk/variables.tf`` file with the new image tag. 
2. The docker image for the Fluent Bit should be available on docker hub. Following are the steps to build and push the Fluent Bit docker image to docker hub:
    1. Change directory: ``cd app-account/fluentbit/image``
    2. Build the docker image: ``docker build -it <image_name>:<image_tag> .``
    3. Push the image to docker hub: ``docker push <image_name>:<image_tag>``
    4. Update the variable ``fluentbit_container_image`` in ``app-account/fluentbit/variables.tf`` file with the new image tag. 
3. This example uses a simple spring boot application as the demo application. The docker image for the application is available as ``xmsandhu/demo-docker:latest``. If you need to deploy your own application, then update the variable ``app_container_image`` in ``app-account/app/variables.tf`` file with the new image tag.  


### Terraform Workspaces
The terraform workspaces are used to apply the script to different environments like development, production, etc. in the same account.

By default, the terraform scripts are applied from the ``default`` workspace.

The user need to create and select each workspace before planning and applying the scripts.

##### Commands:
1. Create a new workspace: `terraform workspace new development`
2. List all the workspaces in the module: `terraform workspace list`
3. Select a particular workspace: `terraform workspace select development`
4. Delete a workspace: `terraform workspace delete development`

The current workspace name can be accessed in the script as ``${terraform.workspace}``.

### Steps to execute the scripts

#### Step 1: Account 1 - Setup the S3 bucket to use as terraform backend
1. Change directory to ``efk-account/s3``
2. Update the bucket name in ``variables.tf``
3. Execute ``terraform init`` to initialize a working directory
4. Execute ``terraform plan`` to create an execution plan
5. Execute ``terraform apply -auto-approve`` to apply the changes

#### Step 2: Account 1 - Setup the EFK cluster
1. Change directory to ``efk-account/efk``
2. Update the value ``terraform.backend.s3.bucket`` in ``provider.tf`` with the bucket name created in Step 1
3. Execute ``terraform init`` to initialize a working directory
4. Execute ``terraform plan`` to create an execution plan
5. Execute ``terraform apply -auto-approve`` to apply the changes

**Outputs:**
- ``es_endpoint`` - The Amazon Elasticsearch Service URL
- ``kibana_endpoint`` - The Kibana URL
- ``fluentd_address`` - The Fluentd address

#### Step 3: Account 2 - Deploy the demo app with fluent bit
1. Change directory to ``app-account``
2. Update the value ``data.terraform_remote_state.efk.config.bucket`` in ``main.tf`` with the bucket name created in Step 1
3. Update the value ``data.terraform_remote_state.efk.config.profile`` to use the Account 1 profile
3. Execute ``terraform init`` to initialize a working directory
4. Execute ``terraform plan`` to create an execution plan
5. Execute ``terraform apply -auto-approve`` to apply the changes

**Outputs:**
- ``app_endpoint`` - The demo application URL

**How to generate logs from the demo application?**

Enter ``<app_endpoint>/<anything>`` to generate a log.
Search for the logs in Kibana using the filter: ``log is <anything>``


### IMPORTANT:
After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. 
Execute ``terraform destroy -auto-approve`` from the directories in the following order: 
1. ``app-account``
2. ``efk-account/efk``
3. ``efk-account/s3``

**Important**: Do not run this command in production unless you want to tear down everything.
