#
# *** IMPORTANT : Never pass your AWS access/secret keys to anyone
#

# TODO: Replace with your keys
provider "aws" {
    access_key = "PUT YOUR ACCESS KEY HERE"
    secret_key = "PUT YOUR SECRET KEY HERE"
    region = "us-east-1"
}

resource "aws_instance" "example" {
    ami = "ami-2757f631"
    instance_type = "t2.micro"
}