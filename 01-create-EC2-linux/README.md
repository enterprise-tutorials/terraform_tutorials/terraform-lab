# In this lesson you will be able to create a ec2 instance

I have set this example up on `us-east-1` region. If you change the region, make sure you change the `ami` as well.

# Important
* This is the only example in the lab that will have access/secret keys in the script. Everything else will be controlled via environment variables

# Steps to execute

### A. Setting AWS keys 
Open `ec2.tf` and set you access + secret keys.

### B. Executing Scripts
1. ``cd`` into script directory
2. ``terraform init`` This is to initialize provider plugins locally
3. ``terraform validate`` will validate the script that you have written
3. ``terraform plan`` will display the settings what we are going to deploy
4. ``terraform apply`` This is to create the instances in AWS

```
After 'terraform apply', the current state of your infrastructure is saved in a file with a .tfstate extension. The file is formatted in JSON and typically you should never need to edit it. It is recommended that this be stored in a central location with a locking mechanism for team collaboration. It may contain secrets in plain text and should be kept safe. It is not recommended to use a version-control for this.
```

After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. Execute ``terraform destroy`` to remove all the resources. **Important**: Do not run this command in production unless you want to tear down everything.
