variable "aws_region" {
    description = "The AWS region where instance should be created."
    default = "us-east-1"
}

variable "aws_profile" {
    description = "AWS profile mentioned in credentials file."
    default = "bird-lion"
}

variable "es_domain" {
    default = "tf-test"
}

variable "es_version" {
    default = "7.1"
}

variable "es_instance_type" {
    default = "t2.small.elasticsearch"
}

variable "ebs_volume_size" {
    description = "Optionally use EBS volumes for data storage by specifying volume size in GB (default 0)"
    type        = number
    default     = 10
}

variable "ebs_volume_type" {
    description = "Storage type of EBS volumes, if used (default gp2)"
    type        = string
    default     = "gp2"
}

variable "es_tag" {
    description = "Tag attached to the domain"
    default     = "TestDomain"
}

/******
  This section is used to
    a) Specify a particular TF version
    b) Keep TF state on S3 (bucket should be manually created before running the scripts)
  Note: You can comment this section if needed.
******/
terraform {
  required_version = "0.12.13"

  backend "s3" {
    bucket = "terraform-labs01"
    key = "tests/terraform/terraform.tfstate"
    region = "us-east-1"
    encrypt = true
  }
}
