provider "aws" {
    profile = "${var.aws_profile}"
    region = "${var.aws_region}"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_elasticsearch_domain" "es" {
    domain_name           = "${var.es_domain}"
    elasticsearch_version = "${var.es_version}"
    cluster_config {
        instance_type = "${var.es_instance_type}"
    }

    advanced_options = {
        "rest.action.multi.allow_explicit_index" = "true"
    }
    access_policies = <<CONFIG
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "es:*",
                "Principal": "*",
                "Effect": "Allow",
                "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.es_domain}/*",
                "Condition": {
                    "IpAddress": {"aws:SourceIp": ["0.0.0.0/0"]}
                }
            }
        ]
    }
    CONFIG

    node_to_node_encryption {
        enabled = "true"
    }

    snapshot_options {
        automated_snapshot_start_hour = 23
    }

    ebs_options{
        ebs_enabled = true
        volume_size = "${var.ebs_volume_size}"
        volume_type = "${var.ebs_volume_type}"
    }

    tags = {
        Domain = "${var.es_tag}"
    }
}

output "es_endpoint" {
  description = "Domain-specific endpoint used to submit index, search, and data upload requests"
  value = aws_elasticsearch_domain.es.endpoint
}

output "kibana_endpoint" {
  description = "Domain-specific endpoint for kibana"
  value = aws_elasticsearch_domain.es.kibana_endpoint
}
