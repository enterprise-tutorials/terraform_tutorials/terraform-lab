# In this lesson you will be able to create a Linux EC2 instance using variables

I have set this example up on `us-east-1` region. If you change the region, make sure you change the `ami` as well.

# Steps to execute

### A. Setting AWS keys 
1. Open `variables.tf` and set your AWS profile. The profile attribute refers to the AWS Config File in ~/.aws/credentials on MacOS and Linux or %UserProfile%\.aws\credentials on a Windows system. It is recommended practice that credentials never be hardcoded into *.tf configuration files. 


### B. Executing Scripts
1. ``cd`` into script directory
2. ``terraform init`` This is to initialize provider plugins locally
3. ``terraform validate`` will validate the script that you have written
3. ``terraform plan`` will display the settings what we are going to deploy
4. ``terraform apply`` This is to create the instances in AWS

After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. Execute ``terraform destroy`` to remove all the resources. **Important**: Do not run this command in production unless you want to tear down everything.

