# In this lesson you will be able to create a ec2 instance and save the state to S3

In the previous examples, after you execute `terraform apply`, you would have noticed 2 files in the folder namely `terraform.tfstate` and `terraform.tfstate.backup`. These hold the state of the infrastructure you created. In this example, the state files will be maintained in S3.

# Steps to execute

### Pre-requsite
* Create a S3 bucket as mentioned in `backend.tf`

### Setting AWS keys 
* Open `variables.tf` & `backend.tf` and set you AWS profile

### Executing Scripts
1. ``cd`` into script directory
2. ``terraform init`` This is to initialize provider plugins locally
3. ``terraform validate`` will validate the script that you have written
3. ``terraform plan`` will display the settings what we are going to deploy
4. ``terraform apply`` This is to create the instances in AWS

After all your tests, to avoid unnecessary costs, do not forget to remove what you have built. Execute ``terraform destroy`` to remove all the resources. **Important**: Do not run this command in production unless you want to tear down everything.

Also remove the S3 bucket that you created
