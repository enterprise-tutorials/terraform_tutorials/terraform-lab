variable "aws_region" {
    description = "The AWS region where instance should be created."
    default = "us-east-1"
}

variable "aws_profile" {
    description = "AWS profile mentioned in credentials file."
    default = "bird-lion"
}

variable "aws_instance_type" {
    description = "Default AWS instance type."
    default = "t2.micro"
}
