terraform {
    backend "s3" {
        bucket = "test-s3-terraform"
        key    = "terraform.tfstate"
        region = "us-east-1"
    }
}